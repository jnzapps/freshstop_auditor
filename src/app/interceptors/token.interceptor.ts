import {
HttpRequest,
HttpHandler,
HttpEvent,
HttpInterceptor,
HttpResponse,
HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {
Router
} from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { ConfigService } from '../services/config.service';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable({
    providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
    consumerKeyEncript: any;
    consumerSecretEncript: any;
    deviceId: string;

    constructor(private router: Router,
        public toastController: ToastController, 
        private config: ConfigService) {
            const deviceInfo = this.getDeviceInfo();
            console.log(deviceInfo);
            this.consumerKeyEncript = this.config.appKey;
            this.consumerSecretEncript = this.config.appSecret;
            this.deviceId = '123456789';
        }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let d = new Date();
        const token = 'test';

        // if (token) {
            request = request.clone({
            setHeaders: {
                // Authorization: token,
                'consumer-key': Md5.hashStr(this.consumerKeyEncript).toString(),
                'consumer-secret': Md5.hashStr(this.consumerSecretEncript).toString(),
                'consumer-nonce': d.getTime().toString(),
                'consumer-device-id': this.deviceId,
                // 'content-type': 'application/json',
                // 'Accept': 'application/json'
            }
            });
        // }
        
        if (!request.headers.has('Content-Type')) {
            request = request.clone({
            setHeaders: {
                'Content-Type': 'application/json'
            }
            });
        }
        
        request = request.clone({
            headers: request.headers.set('Accept', 'application/json')
        });
        
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                console.log('event--->>>', event);
            }
            return event;
            }),
            catchError((error: HttpErrorResponse) => {
            if (error.status === 401) {
                if (error.error.success === false) {
                this.presentToast('Login failed');
                } else {
                this.router.navigate(['login']);
                }
            }
            return throwError(error);
            }));
        // return next.handle(request);
        }

    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
    toast.present();
    }

    async getDeviceInfo(){
    //    return await Device.getInfo(); 
    }
}