import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    // path: '', redirectTo: 'login', pathMatch: 'full'
    path: '', redirectTo: 'login', pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'tasks',
    loadChildren: () => import('./pages/tasks/tasks.module').then( m => m.TasksPageModule)
  },
  {
    path: 'projects',
    loadChildren: () => import('./pages/projects/projects.module').then( m => m.ProjectsPageModule)
  },
  {
    path: 'tasks-add',
    loadChildren: () => import('./pages/tasks-add/tasks-add.module').then( m => m.TasksAddPageModule)
  },
  {
    path: 'projects-add',
    loadChildren: () => import('./pages/projects-add/projects-add.module').then( m => m.ProjectsAddPageModule)
  },
  {
    path: 'invoices',
    loadChildren: () => import('./pages/invoices/invoices.module').then( m => m.InvoicesPageModule)
  },
  {
    path: 'estimates',
    loadChildren: () => import('./pages/estimates/estimates.module').then( m => m.EstimatesPageModule)
  },
  {
    path: 'task-view',
    loadChildren: () => import('./pages/task-view/task-view.module').then( m => m.TaskViewPageModule)
  },
  {
    path: 'project-view',
    loadChildren: () => import('./pages/project-view/project-view.module').then( m => m.ProjectViewPageModule)
  },
  {
    path: 'covidform',
    loadChildren: () => import('./pages/covidform/covidform.module').then( m => m.CovidformPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'covidresult',
    loadChildren: () => import('./pages/covidresult/covidresult.module').then( m => m.CovidresultPageModule)
  },
  {
    path: 'auditquestions',
    loadChildren: () => import('./pages/auditquestions/auditquestions.module').then( m => m.AuditquestionsPageModule)
  },
  {
    path: 'audits',
    loadChildren: () => import('./pages/audits/audits.module').then( m => m.AuditsPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
