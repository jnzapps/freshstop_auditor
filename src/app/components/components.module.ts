import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { AlertComponentComponent } from './alerts/alert-component/alert-component.component';

@NgModule({
  declarations: [
    AlertComponentComponent,
  ],
  imports: [
    IonicModule,
    CommonModule
  ],
  exports: [
    AlertComponentComponent,
  ]
})
export class ComponentsModule { }
