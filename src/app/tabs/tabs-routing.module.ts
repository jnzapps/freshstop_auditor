import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        // loadChildren: () => import('../pages/covidform/covidform.module').then(m => m.CovidformPageModule)
		// loadChildren: () => import('../pages/auditquestions/auditquestions.module').then(m => m.AuditquestionsPageModule)
		loadChildren: () => import('../pages/audits/audits.module').then(m => m.AuditsPageModule)
      },
      {
        path: 'tab2',
        // loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
        loadChildren: () => import('../pages/tasks/tasks.module').then(m => m.TasksPageModule)
      },
      {
        path: 'tab3',
        // loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
        loadChildren: () => import('../pages/projects/projects.module').then(m => m.ProjectsPageModule)
      },
      {
        path: 'tab4',
        loadChildren: () => import('../pages/account/account.module').then(m => m.AccountPageModule)
      },
      /*
      {
        path: 'tab3',
        loadChildren: () => import('../pages/covidform/covidform.module').then(m => m.CovidformPageModule)
      },
      */
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
