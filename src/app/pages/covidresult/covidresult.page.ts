import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-covidresult',
  templateUrl: './covidresult.page.html',
  styleUrls: ['./covidresult.page.scss'],
})
export class CovidresultPage implements OnInit {

  viewSection = '';
  result = '';
  constructor(navParams: NavParams, public modalCtrl: ModalController, public dataService: GlobalService, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public toastCtrl: ToastController) { 
    this.result = navParams.get('result');  
    this.viewSection = navParams.get('result');
  }

  ngOnInit() {
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }
}
