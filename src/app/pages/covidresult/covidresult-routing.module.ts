import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CovidresultPage } from './covidresult.page';

const routes: Routes = [
  {
    path: '',
    component: CovidresultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CovidresultPageRoutingModule {}
