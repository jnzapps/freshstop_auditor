import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CovidresultPageRoutingModule } from './covidresult-routing.module';

import { CovidresultPage } from './covidresult.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CovidresultPageRoutingModule
  ],
  declarations: [CovidresultPage]
})
export class CovidresultPageModule {}
