import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CovidresultPage } from './covidresult.page';

describe('CovidresultPage', () => {
  let component: CovidresultPage;
  let fixture: ComponentFixture<CovidresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidresultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CovidresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
