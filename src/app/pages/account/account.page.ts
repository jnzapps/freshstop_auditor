import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  constructor(public authService: AuthService, public dataService: GlobalService) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
    this.dataService.clearUserData();
  }

}
