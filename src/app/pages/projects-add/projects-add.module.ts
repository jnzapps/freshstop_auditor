import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjectsAddPageRoutingModule } from './projects-add-routing.module';

import { ProjectsAddPage } from './projects-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProjectsAddPageRoutingModule
  ],
  declarations: [ProjectsAddPage]
})
export class ProjectsAddPageModule {}
