import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsAddPage } from './projects-add.page';

const routes: Routes = [
  {
    path: '',
    component: ProjectsAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsAddPageRoutingModule {}
