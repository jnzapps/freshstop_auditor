import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuditquestionsPageRoutingModule } from './auditquestions-routing.module';

import { AuditquestionsPage } from './auditquestions.page';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AuditquestionsPageRoutingModule,
	SignaturePadModule
  ],
  declarations: [AuditquestionsPage]
})
export class AuditquestionsPageModule {}
