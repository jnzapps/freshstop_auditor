import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuditquestionsPage } from './auditquestions.page';

const routes: Routes = [
  {
    path: '',
    component: AuditquestionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditquestionsPageRoutingModule {}
