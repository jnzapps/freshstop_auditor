import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { Router, NavigationExtras,ActivatedRoute, NavigationEnd } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { GlobalService } from 'src/app/services/global.service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-auditquestions',
  templateUrl: './auditquestions.page.html',
  styleUrls: ['./auditquestions.page.scss'],
})
export class AuditquestionsPage implements OnInit {
  @ViewChild(SignaturePad, {static: false}) signaturePad: SignaturePad;
  // @Input() form: FormGroup;
  questionForm: FormGroup = new FormGroup({});
  site_selected: FormGroup = new FormGroup({});
  // form: FormGroup;

  public audit_id: number = 0;
  public audit_status = 0;
  public questions = [];
  public images = [];
  public signatures = [];
  public site;
  public sites;

  // for completed audits
  public site_id = 0;
  public site_name = null;
  public user_id: number;

  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 300,
    'canvasHeight': 125
  };
  
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient, 
    private config: ConfigService, 
    private router: Router,
    public toastCtrl: ToastController,
    public alertController: AlertController,
    private dataService: GlobalService,
    public route: ActivatedRoute,
    private camera: Camera,
    private file: File
	) { 

  }

  ngOnInit() {
	   this.getQuestions();
  }
  
  ngAfterViewInit() {
    // this.signaturePad is now available
    // this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
    // this.signaturePad.set(this.signaturePadOptions); // set szimek/signature_pad options at runtime
    // this.signaturePads.clear(); // invoke functions from szimek/signature_pad API
    // this.signupSlider.lockSwipes(true);

  }
  
  drawComplete(signature_id: number) {
    // will be notified of szimek/signature_pad's onEnd event
    // console.log(this.signaturePad.toDataURL());
    this.signatures.push({id: signature_id, data: this.signaturePad.toDataURL()});
  }
  
  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }
	
  // async getQuestions (data) {
	  
  //   return await this.http.get(this.config.url + 'audit_questions/' + data.id).subscribe((result:any) => {
  //      this.questions = result; 
  //      return true;
  //   });
  // }
  
  async getQuestions(){
    this.dataService.getSites().subscribe((data: any) => {
      this.sites = data.data;
      // console.log('Sites', this.sites);
    });
    
    this.route.queryParams.subscribe(params => {
      let parameters = JSON.parse(params.data);
      this.audit_id = parameters.id;
      this.site = { id: parameters.site_id };
      this.user_id = parameters.user_id; 
      this.audit_status = parameters.status;
     
      console.log(this.site_id);
      
      this.dataService.getQuestions(parameters).subscribe((data: any) => {
  
        let questions = data.questions;
        this.questions = questions;
        this.site_name = data.site_name.site_name;
  
        // console.log('Questions', questions);
  
        let group: any = {};
  
        questions.forEach(field => {
          // group[field.id] = field.question_required 
          //   ? new FormControl(field.question || '', Validators.required)
          //   : new FormControl(field.question || '');
          group[field.id] = field.question_required 
            ? new FormControl('', Validators.required)
            : new FormControl('');
        });
        
        this.questionForm = new FormGroup(group);
  
       });
    });
  }

  takePicture(questionId) {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: true,
      correctOrientation: true,
    }
    
    this.camera.getPicture(options).then((imageData) => {
    // imageData is either a base64 encoded string or a file URI
    // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;

      //needs to import file plugin
      //split the file and the path from FILE_URI result
      // let filename = imageData.substring(imageData.lastIndexOf('/')+1);
      // let path =  imageData.substring(0,imageData.lastIndexOf('/')+1);
      //then use the method reasDataURL  btw. var_picture is ur image variable
      // let var_picture = null;
      // this.file.readAsDataURL(path, filename).then(res=> var_picture = res  );

      this.images.push({question: questionId, image: base64Image});
    }, (err) => {
    // Handle error
    });
  }

  submit(status: number)
  {
    const data = {  
                  site_id: this.site.id, 
                  audit_id: this.audit_id, 
                  user_id: this.dataService.userData.user_id, 
                  question_data: this.questionForm.value, 
                  signatures: this.signatures, 
                  images: this.images, 
                  audit_status: status 
                };

    this.http.post(this.config.url+'audit_answers', data).subscribe((result: any) => {
      if(result.status == 1){
        this.show_toast(result.message);
        this.router.navigate(['home/tabs/tab1'],{ replaceUrl: true });
      }else{
        this.show_toast(result.message);
      }
    });
  }

  compareWithFn(o1, o2): boolean {
    return o1.id === o2.id; 
  }

  async show_toast(header: string) {
    let toast = await this.toastCtrl.create({
      header: header,
      duration: 3000
    });
    toast.present();
  }

}
