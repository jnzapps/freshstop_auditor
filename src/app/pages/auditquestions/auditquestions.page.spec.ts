import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuditquestionsPage } from './auditquestions.page';

describe('AuditquestionsPage', () => {
  let component: AuditquestionsPage;
  let fixture: ComponentFixture<AuditquestionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditquestionsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuditquestionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
