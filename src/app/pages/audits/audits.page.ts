import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { Router, NavigationExtras } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-audits',
  templateUrl: './audits.page.html',
  styleUrls: ['./audits.page.scss'],
})
export class AuditsPage implements OnInit {

  public audits = [];
  
  constructor(private http: HttpClient, 
    private config: ConfigService, 
    private router: Router,
    public toastController: ToastController,
    public alertController: AlertController,
    private dataService: GlobalService) { }

  ngOnInit() {
	   this.getAudits();
  }

  getAudits () {
     this.http.get(this.config.url + 'audits/' + this.dataService.userData.user_id).subscribe((result:any) => { this.audits = result.data.audits });
  }
  
  view(data: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(data)
      }
    }
    this.router.navigate(['auditquestions'], navigationExtras);
  }

}
