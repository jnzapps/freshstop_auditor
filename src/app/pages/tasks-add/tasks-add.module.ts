import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TasksAddPageRoutingModule } from './tasks-add-routing.module';

import { TasksAddPage } from './tasks-add.page';
import { ComponentsModule } from '../../components/components.module';

ComponentsModule
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    TasksAddPageRoutingModule
  ],
  declarations: [TasksAddPage]
})
export class TasksAddPageModule {}
