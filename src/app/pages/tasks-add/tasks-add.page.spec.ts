import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TasksAddPage } from './tasks-add.page';

describe('TasksAddPage', () => {
  let component: TasksAddPage;
  let fixture: ComponentFixture<TasksAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TasksAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TasksAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
