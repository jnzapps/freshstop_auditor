import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TasksAddPage } from './tasks-add.page';

const routes: Routes = [
  {
    path: '',
    component: TasksAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TasksAddPageRoutingModule {}
