import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, ReactiveFormsModule  } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { Router, NavigationExtras } from '@angular/router';
import { AlertService } from 'src/app/services/alert/alert.service';
import { config } from 'process';

@Component({
  selector: 'app-tasks-add',
  templateUrl: './tasks-add.page.html',
  styleUrls: ['./tasks-add.page.scss'],
})
export class TasksAddPage implements OnInit {

  public taskForm: FormGroup;

  public submitAttempt: boolean = false;


  constructor(private http: HttpClient, 
    private config: ConfigService,
    private formBuilder: FormBuilder,
    public toastController: ToastController,
    public alertCtrl: AlertService) { }

  ngOnInit() {
    this.taskForm = this.formBuilder.group({
      'subject' : ['', Validators.required],
      'priority' : ['', Validators.required],
      'description' : ['', Validators.required],
      'dueDate' : ['', Validators.required],
    });
  }

  onFormSubmit(form: NgForm) {
    this.http.post(`${this.config.url}create_task`, form)
      .subscribe((data:any) => {
        if (data.success == 1) {
          this.alertCtrl.presentAlert({header: 'Success', subHeader: 'saved', message: data.message, buttons: ['OK']}).then(res => res.present());
        } else {
          this.alertCtrl.presentAlert({header: 'Unsuccessful', message: data.message, buttons: ['OK']}).then(res => res.present());
        }
      }, (err) => {
        console.log(err);
      });
    
  }

}
