import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { Router, NavigationExtras } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
  providers: [DatePipe]
})
export class TasksPage implements OnInit {

  public tasks = [];
  public currentDate = Date();

  constructor(private http: HttpClient, 
    private config: ConfigService, 
    private router: Router,
    public toastController: ToastController,
    public alertController: AlertController,
    private datePipe: DatePipe,
    private dataService: GlobalService
    ) { 
   
    this.currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  ngOnInit() {
    this.getTasks();
  }

  getTasks () {
     this.http.get(this.config.url + 'tasks_get/' + this.dataService.userData.user_id).subscribe((result:any) => { this.tasks = result });
  }

  add() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify([])
      }
    }
    this.router.navigate(['tasks-add'], navigationExtras);
  }

  view(data: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(data)
      }
    }
    this.router.navigate(['task-view'], navigationExtras);
  }
}
