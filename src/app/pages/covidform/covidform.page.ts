import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, NgForm, Validators, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { ToastController, ModalController, IonSlides } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { GlobalService } from 'src/app/services/global.service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { CovidresultPage } from '../covidresult/covidresult.page';

@Component({
  selector: 'app-covidform',
  templateUrl: './covidform.page.html',
  styleUrls: ['./covidform.page.scss'],
})
export class CovidformPage implements OnInit {
  @ViewChild(SignaturePad, {static: false}) signaturePad: SignaturePad;
  @ViewChild('signupSlider', {static: false}) signupSlider: IonSlides;
  slidesOptions = { initialSlide: 0 }

  public covidForm: FormGroup;
  public slideTwoForm: FormGroup;
  public slideThreeForm: FormGroup;
  public submitAttempt: boolean = false;
  activeSlide = 0;

  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 300,
    'canvasHeight': 125
  };

 
  public chills;
  public sore_throat;
  public cough;
  public shortness_of_breath;
  public myalgia_body_pains;
  public diarrhoea;

  constructor(private http: HttpClient, 
    private config: ConfigService,
    private formBuilder: FormBuilder,
    public toastController: ToastController,
    public alertCtrl: AlertService,
    public dataService: GlobalService,
    public modalCtrl: ModalController) { }
    
    ngOnInit() {
      this.covidForm = this.formBuilder.group({
        'mask' : ['', Validators.required],
        'temperature' : ['', Validators.required],
      });
      this.slideTwoForm = this.formBuilder.group({
        'chills' : [false],
        'sore_throat' : [false],
        'cough' : [false],
        'shortness_of_breath' : [false],
        'myalgia_body_pains' : [false],
        'diarrhoea' : [false],
      });
      this.slideThreeForm = this.formBuilder.group({
        'signaturePadData' : [''],
      });
    }
    
    onFormSubmit(form: NgForm) {
     /*
      let formData = {  ...form, user_id: this.dataService.userData.user_id, signature: this.signaturePad.toDataURL() };
      console.log(formData);
      this.http.post<any>(`${this.config.url}add_covid`, formData)
        .subscribe((data:any) => {
          if (data.status == 'success') {
          //   this.alertCtrl.presentAlert({
          //       header: 'Success', subHeader: 'saved', message: data.message, 
          //       buttons: [{
          //         text: 'OK'
          //       }]
          // }).then(res => res.present());
          this.showCovidResult('safe');
          this.clearForm();
          } else {
            // this.alertCtrl.presentAlert({header: 'Unsuccessful', message: data.message, buttons: ['OK']}).then(res => res.present());
            this.showCovidResult('unsafe');
          }
        }, (err) => {
          console.log(err);
        });
        */
    }

    ngAfterViewInit() {
      // this.signaturePad is now available
      // this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
      // this.signaturePad.set(this.signaturePadOptions); // set szimek/signature_pad options at runtime
      this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
      // this.signupSlider.lockSwipes(true);
      // this.signupSlider.getSwiper(on
    }
   
    drawComplete() {
      // will be notified of szimek/signature_pad's onEnd event
      console.log(this.signaturePad.toDataURL());
    }
   
    drawStart() {
      // will be notified of szimek/signature_pad's onBegin event
      console.log('begin drawing');
    }

    clearForm() {
      this.covidForm.reset();
      this.slideTwoForm.reset();
      this.signaturePad.clear();
      this.activeSlide = 0;
      console.log(this.activeSlide);
    }

    async showCovidResult(result: string)
    {
      console.log('showResults');
      let modal = await this.modalCtrl.create({
          component: CovidresultPage,
          componentProps: {
            result: result
          }
        });
        modal.onDidDismiss()
        .then(() => {
          // console.log('modalDismiss', udpateOrder);
          this.signupSlider.lockSwipes(false);
          this.activeSlide = 0;
          this.signupSlider.slideTo(0);
          this.signupSlider.lockSwipes(true);
        });
    
        modal.present();
    }

    next(){
      this.signupSlider.lockSwipes(false);
      this.signupSlider.slideNext();
      this.signupSlider.lockSwipes(true);
      this.signupSlider.getActiveIndex().then((index) => 
      {
        this.activeSlide = index;
        console.log(index);
      });
    }
  
    prev(){
      this.signupSlider.lockSwipes(false);
      this.signupSlider.slidePrev();
      this.signupSlider.lockSwipes(true);
      this.signupSlider.getActiveIndex().then((index) => 
      {
        this.activeSlide = index;
        console.log(index);
      });
    }
  
    save(){
      this.submitAttempt = true;
      console.log('save forms');
      if(!this.covidForm.valid){
          this.signupSlider.lockSwipes(false);
          this.signupSlider.slideTo(0);
          this.signupSlider.lockSwipes(true);
      } 
      else if(!this.slideTwoForm.valid){
        this.signupSlider.lockSwipes(false);
          this.signupSlider.slideTo(1);
          this.signupSlider.lockSwipes(true);
      }
      else if(!this.slideThreeForm.valid){
        this.signupSlider.lockSwipes(false);
        this.signupSlider.slideTo(2);
        this.signupSlider.lockSwipes(true);
      }
      else {
        console.log("success!")
        console.log(this.covidForm.value);
        console.log(this.slideTwoForm.value);
        this.signupSlider.getActiveIndex().then((index) => 
        {
          this.activeSlide = index;
          console.log(index);
        });
        
        // let data = {section1 : this.registerForm.value, section2: this.slideTwoForm.value };
        let data = { ...this.covidForm.value, ...this.slideTwoForm.value, signature: this.signaturePad.toDataURL(), user_id: this.dataService.userData.user_id };
        this.submitForm(data);
      }
    }

    submitForm(formData){
      this.http.post<any>(`${this.config.url}add_covid`, formData)
      .subscribe((data:any) => {
        if (data.status == 'success') {
        //   this.alertCtrl.presentAlert({
        //       header: 'Success', subHeader: 'saved', message: data.message, 
        //       buttons: [{
        //         text: 'OK'
        //       }]
        // }).then(res => res.present());
        this.showCovidResult('safe');
        this.clearForm();
        } else if(data.status == 'fail') {
          // this.alertCtrl.presentAlert({header: 'Unsuccessful', message: data.message, buttons: ['OK']}).then(res => res.present());
          this.showCovidResult('unsafe');
        } else {
          this.alertCtrl.presentAlert({header: 'Error', message: data.message, buttons: ['OK']}).then(res => res.present());
        }
      }, (err) => {
        console.log(err);
      });
    }

    onIonDrag(event){
      console.log(event);
      this.signupSlider = event;
    }
} 
