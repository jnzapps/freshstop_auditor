import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CovidformPage } from './covidform.page';

const routes: Routes = [
  {
    path: '',
    component: CovidformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CovidformPageRoutingModule {}
