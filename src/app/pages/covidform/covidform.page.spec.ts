import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CovidformPage } from './covidform.page';

describe('CovidformPage', () => {
  let component: CovidformPage;
  let fixture: ComponentFixture<CovidformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CovidformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
