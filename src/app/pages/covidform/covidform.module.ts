import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CovidformPageRoutingModule } from './covidform-routing.module';

import { CovidformPage } from './covidform.page';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CovidformPageRoutingModule,
    SignaturePadModule,
  ],
  declarations: [CovidformPage]
})
export class CovidformPageModule {}
