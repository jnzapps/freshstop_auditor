import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { NavigationExtras, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.page.html',
  styleUrls: ['./projects.page.scss'],
  providers: [DatePipe]
})
export class ProjectsPage implements OnInit {

  public projects = [];
  public currentDate = Date();

  constructor(private http: HttpClient, private config: ConfigService, private router: Router, private datePipe: DatePipe) { 
    this.getProjects();
    this.currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    console.log(this.currentDate);
  }

  ngOnInit() {
  }

  getProjects () {
    this.http.get(this.config.url + 'projects_get/1').subscribe((result:any) => { this.projects = result });
  }

  add() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify([])
      }
    }
    this.router.navigate(['projects-add'], navigationExtras);
  }

  view(data: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(data)
      }
    }
    this.router.navigate(['project-view'], navigationExtras);
  }
}
