import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
 
    loginForm: FormGroup;
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off-outline';
    loading;

    constructor(private formBuilder: FormBuilder,
      private router: Router,
      private authService: AuthService,
      public toastController: ToastController,
      public alertCtrl: AlertController,
      public loadingCtrl: LoadingController,
      public dataService: GlobalService
      ) { }
  
    ngOnInit() {
      this.loginForm = this.formBuilder.group({
        'email' : ['jamie@jnz.co.za', Validators.required],
        'password' : ['password', Validators.required],
        'is_staff' : [true]
      });
    }
  
    onFormSubmit(form: NgForm) {
      this.presentLoading();

      this.authService.login(form)
        .subscribe(res => {
          if (res.status == 'success') {
            console.log(res);
            this.authService.setLogin({user_id: res.data.id, name: res.data.full_name, type: 'employee', status: res.data.status, email: res.data.email, role: 'employee', profile_image: res.data.profile_image, profile_image_url: res.data.profile_image_url}).subscribe(result => {
              if(result){
                this.dataService.loadUserData();
                // if(result.role == 'Retailer-Admin'){
                //   this.router.navigate(['admin/dashboard'],{ replaceUrl: true });
                // }
                // if(result.role == 'Retailer-Staff'){
                //   this.router.navigate(['retailer/dashboard'],{ replaceUrl: true });
                // }
                // console.log(result);
                this.closeLoading();
                this.router.navigate(['home/tabs/tab1'],{ replaceUrl: true });
                // console.log('routing to tabs');
              }
            });
            
          }else{
            this.closeLoading();
            this.presentToast(res.message);
          }
        }, (err) => {
          this.closeLoading();
          console.log(err);
        });
    }
  
    register() {
      this.router.navigate(['register']);
    }
  
    async presentToast(msg) {
      const toast = await this.toastController.create({
        message: msg,
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }
    
    async presentLoading() {
      const loading = await this.loadingCtrl.create({
        cssClass: 'my-custom-class',
        message: 'Please wait...',
      });
      await loading.present();
    }

    closeLoading(){
      this.loadingCtrl.dismiss();
    }

    hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off-outline' ? 'eye-outline' : 'eye-off-outline';
    }
}
