import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, ReactiveFormsModule  } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  // https://www.joshmorony.com/advanced-forms-validation-in-ionic-2/
  @ViewChild('signupSlider', {static: false}) signupSlider;

	public registerForm: FormGroup;
  public slideTwoForm: FormGroup;
  
  public submitAttempt: boolean = false;

  // registerForm: FormGroup;
  titles = [
    'Mr',
    'Mrs',
    'Ms',
    'Dr',
    'Proff'
  ]
  provinces = [
      'Eastern Cape',
      'Free State',
      'Western Cape',
      'Gauteng',
      'KwaZulu Natal',
      'Limpopo',
      'Mpumalanga',
      'Northern Cape',
      'North West'
    ];
  professions = [
    'Medical Doctor',
    'Nursing Staff',
    'Pharmacy Staff',
    'Sonargrapher',
    'Radiographer',
    'Medical Scientist',
    'Dentist',
  ]
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    public toastController: ToastController,
    public alertController: AlertController
    ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        'title' : ['Mr', Validators.required],
        'firstname' : ['first', Validators.required],
        'lastname' : ['test', Validators.required],
        'cellphone' : ['078', Validators.required],
        'email' : ['test', Validators.required],
        'id_number' : ['852155424585', Validators.required],
        'password' : ['test', Validators.required]
      });

    // this.registerForm = this.formBuilder.group({
    //   'firstname' : ['first', Validators.required],
    //   'lastname' : ['last', Validators.required],
    //   'cellphone' : ['078', Validators.required],
    //   'email' : ['test', Validators.required],
    //   'province' : ['test', Validators.required],
    //   'profession' : ['teset', Validators.required],
    //   'registration_number' : ['test', Validators.required],
    //   'password' : ['test', Validators.required]
    // });

    // this.slideTwoForm = this.formBuilder.group({
    //   firstNames: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
    //   lastNames: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
    //   age: ['']
    // });
  }

  onFormSubmit(form: NgForm) {
    this.authService.register(form)
      .subscribe(data => {
        // this.presentAlert('Register Successfully', 'Please login with your new username and password');
        if (data.success == 1) {
          this.presentAlert('Registration Successfully', data.message);
        } else {
          this.presentAlert('Registration Error', data.message, false);
        }
      }, (err) => {
        console.log(err);
      });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  async presentAlert(header, message, status: boolean = true) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [{
          text: 'OK',
          handler: () => {
            if(status){
              this.router.navigate(['login']);
            }
          }
        }]
    });

    await alert.present();
  }

  next(){
    this.signupSlider.slideNext();
  }

  prev(){
      this.signupSlider.slidePrev();
  }

  save(){
    this.submitAttempt = true;
    console.log('save forms');
    if(!this.registerForm.valid){
        this.signupSlider.slideTo(0);
    } 
    else if(!this.slideTwoForm.valid){
        this.signupSlider.slideTo(1);
    }
    else {
      console.log("success!")
      console.log(this.registerForm.value);
      console.log(this.slideTwoForm.value);

      // let data = {section1 : this.registerForm.value, section2: this.slideTwoForm.value };
      let data = { ...this.registerForm.value, ...this.slideTwoForm.value };
      console.log(data);
      this.register(data);
    }
  }

  register(data) {
    this.authService.register(data)
      .subscribe(data => {
        // this.presentAlert('Register Successfully', 'Please login with your new username and password');
        if (data.success == 1) {
          this.presentAlert('Registration Successfully', data.message);
        } else {
          this.presentAlert('Registration Error', data.message, false);
        }
      }, (err) => {
        console.log(err);
      });
  }

}