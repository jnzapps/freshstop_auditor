import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  siteProtocol = 'http://';
  // siteProtocol = 'https://';
  siteUrl = 'auditapp.jnzsoftware.co.za/';
  // siteUrl = '127.0.0.1:8000/';
  siteSuffix = 'api/';
   
  public url: string = this.siteProtocol + this.siteUrl + this.siteSuffix;
  public imgUrl: string = this.siteUrl + "";

  public appKey: string = "6df56cf915318431043dd7a75d";
  public appSecret: string = "95032b42153184310488f5fb8f";

  public currency;
  public ticket_priorities;

  constructor(public http: HttpClient) { }

  public appSettings() {
    let d = new Date();
    let heads = new HttpHeaders({
      'consumer-key': Md5.hashStr(this.appKey).toString(),
      'consumer-secret': Md5.hashStr(this.appSecret).toString(),
      'consumer-nonce': d.getTime().toString(),
      'consumer-device-id': '123456', //'device id of the app'
    });

    let options = { headers: heads };

    return new Promise(resolve => {
      this.http.get(this.url + 'app_setting', options).subscribe((data: any) => {
        var settings = data;
        this.ticket_priorities = settings.ticket_priorities;
        console.log(data);
        // this.fbId = settings.facebook_app_id;
        // this.address = settings.address + ', ' + settings.city + ', ' + settings.state + ' ' + settings.zip + ', ' + settings.country;
        // this.email = settings.contact_us_email;
        // this.latitude = settings.latitude;
        // this.longitude = settings.longitude;
        // this.phoneNo = settings.phone_no;
        // this.pushNotificationSenderId = settings.fcm_android_sender_id;
        // this.lazyLoadingGif = settings.lazzy_loading_effect;
        // this.newProductDuration = settings.new_product_duration;
        // this.notifText = settings.notification_text;
        // this.notifTitle = settings.notification_title;
        // this.notifDuration = settings.notification_duration;
        // this.currency = settings.currency_symbol;
        // this.cartButton = settings.cart_button;
        // this.footerShowHide = settings.footer_button;
        // this.setLocalNotification();
        // this.appName = settings.app_name;
        // this.homePage = settings.home_style;
        // this.categoryPage = settings.category_style;
        // this.siteUrl = settings.site_url;
        // this.introPage = settings.intro_page;
        // this.myOrdersPage = settings.my_orders_page;
        // this.newsPage = settings.news_page;
        // this.wishListPage = settings.wish_list_page;
        // this.shippingAddressPage = settings.shipping_address_page;
        // this.aboutUsPage = settings.about_us_page;
        // this.contactUsPage = settings.contact_us_page;
        // this.editProfilePage = settings.edit_profile_page;
        // this.packgeName = settings.package_name;
        // this.settingPage = settings.setting_page;
        // this.admob = settings.admob;
        // this.admobBannerid = settings.ad_unit_id_banner;
        // this.admobIntid = settings.ad_unit_id_interstitial;
        // this.googleAnalaytics = settings.google_analytic_id;
        // this.rateApp = settings.rate_app;
        // this.shareApp = settings.share_app;
        // this.fbButton = settings.facebook_login;
        // this.googleButton = settings.google_login;
        // this.notificationType = settings.default_notification;
        // this.onesignalAppId = settings.onesignal_app_id;
        // this.onesignalSenderId = settings.onesignal_sender_id;
        // this.admobIos = settings.ios_admob;
        // this.admobBanneridIos = settings.ios_ad_unit_id_banner;
        // this.admobIntidIos = settings.ios_ad_unit_id_interstitial;
        // this.defaultIcons = (settings.app_icon_image == "icon") ? true : false;
        // this.wallet_validation_amount = parseFloat(settings.wallet_minimum_validation);
        // this.wallet_minimum_cashout = parseFloat(settings.wallet_minimum_amount);
        // this.banks = settings.banks;
        // this.maintenance_mode = settings.maintenance_mode;
        // this.app_version_android = settings.app_version_android;
        // this.app_version_ios = settings.app_version_ios;
        // this.show_update_available = settings.show_update_available;
        // this.maintenance_message = settings.maintenance_message;
        // this.update_available_message = settings.update_available_message;
        // this.show_news = settings.show_news;
        // this.news_heading = settings.news_heading;
        // this.news_message = settings.news_message;
        // this.news_data = settings.news;
        // this.instore_redemptions_enabled = settings.instore_redemptions_enabled;
        
        resolve();
      });
    });
  }
}
