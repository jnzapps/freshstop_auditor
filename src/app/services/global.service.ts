import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const USER_DATA = 'auth-data';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  public userData: { [k: string]: any } = {};
  public loaded: boolean = false;
  public sites: any = [];

  constructor(private storage: Storage, private http: HttpClient, private config: ConfigService) {
    this.loadUserData();
    // this.getSites();
  }

   load(): Promise<boolean> {
     return new Promise((resolve) => {
      this.storage.get(USER_DATA).then(res =>{
        if (res != null || res != undefined) this.userData = res;
        console.log('loadUserData', this.userData);
      });
      this.loaded = true;
      console.log('isLoaded', this.loaded);
      resolve(true);
    });
  }

  loadRedemptions(): Observable<any> {
    return this.http.post(this.config.url + 'site_redemptions', { user_id: this.userData.user_id }).pipe(
      tap(_ => this.log('loadRedemptions')),
      catchError(this.handleError('loadRedemptions', []))
    );
  }

  async loadUserData() {
    console.log('loadingUserData');
     return await this.storage.get(USER_DATA).then(res => {
      if (res != null || res != undefined) this.userData = res;
      console.log('loadedUserData', this.userData);
    });
  }

  async getUserData(): Promise<any> {
    return await this.userData;
  }

  getNameFirstLetter() {
    return this.userData.name.charAt(0);
  }

  clearUserData() {
    this.userData = {};
  }

  getQuestions(data): Observable<any> { 
    return this.http.get(this.config.url + 'audit_questions/' + data.id).pipe();
  }

  getSites(): Observable<any> {
    // this.http.get(this.config.url + 'audit_sites').subscribe((result:any) => {
    //   this.sites = result.data;
    // });
    return this.http.get(this.config.url + 'audit_sites').pipe();
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }
  
}
