import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, UrlTree } from '@angular/router';
import { AuthService } from './auth.service';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot) {
    const expectedRole = route.data.role;
    console.log('expectedRole', expectedRole);
    // return this.authService.isAuthenticated();
    return this.authService.user.pipe(
      take(1),
      map(user => {
        // console.log('AuthGuard map', user.data.admin_type_name);
        console.log('AuthGuard map', user.role);
        if(user){
        const role = user.role;
        if(expectedRole == role){
          return true;
        }else{
          return this.router.parseUrl('/login');
        }
      }else{
        return this.router.parseUrl('/login');
      }
      })
    )
  }
}
