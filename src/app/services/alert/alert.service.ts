import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private alert;

  constructor(private alertCtrl: AlertController) { }

  presentAlert(data: any) {
    const alert = this.alertCtrl.create({
      header: data.header,
      subHeader: data.subHeader,
      message: data.message,
      mode: 'ios',
      animated: true,
      backdropDismiss: false,
      translucent: true,
      buttons: data.buttons
    });
    // alert.then(result => result.present());
    // alert.catch(ex => console.log(ex));
    return alert;
  }

  doAction() {

  }

  showAlert(){
    this.alert.present();
  }

  dismissAlert() {
    if(this.alert)
      console.log(this.alert);
      this.alert.dismiss();
  }
  
}
