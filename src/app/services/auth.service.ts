import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, tap, filter } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { GlobalService } from './global.service';

const TOKEN_KEY = 'auth-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<any>;
  apiUrl = this.config.url; 
  // authenticationState = new BehaviorSubject(false);
  authenticationState = new BehaviorSubject(null);
  
  constructor(
      private http: HttpClient, 
      private config: ConfigService,
      private plt: Platform, 
      private storage: Storage,
      private router:Router,
      private dataService: GlobalService
    ) {
    this.plt.ready().then(() => {
      this.checkToken();
      this.user = this.authenticationState.asObservable().pipe(
        filter(response => response)
      )
    })
  }

  login (data): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'login', data)
      .pipe(
        tap(_ => this.log('login')),
        catchError(this.handleError('login', []))
      );
  //  this.http.post<any>(this.apiUrl + 'retailer_login', data)
  //     .subscribe(res => {
  //       this.storage.set(TOKEN_KEY, res);
  //       this.authenticationState.next(res);
  //     });
  }
/*
  logout (): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'logout')
      .pipe(
        tap(_ => this.log('logout')),
        catchError(this.handleError('logout', []))
      );
  }
*/
logout() {
  // return this.storage.get(TOKEN_KEY).then(() => { 
    // this.authenticationState.next(false);
  // });
    this.authenticationState.next(null);
    this.storage.remove(TOKEN_KEY);
    // this.router.navigateByUrl('/login');
    this.router.navigate(['login'],{ replaceUrl: true });
}

  register (data): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'register', data)
      .pipe(
        tap(_ => this.log('register')),
        catchError(this.handleError('register', []))
      );
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  checkToken() {
    // return this.storage.get(TOKEN_KEY).then(res => { 
     console.log('checkingToken');
    this.storage.get(TOKEN_KEY).then(res => { 
      if(res){
        console.log('checkToken valid');
        // this.authenticationState.next(true);
        this.authenticationState.next(res);
      }else{
        // this.authenticationState.next(false);
        console.log('checkToken empty');
        this.authenticationState.next({ email: null, role: null, admin_type_name: null});
      }
    })
  }

  setLogin(userData): Observable<any> {
    
    // this.storage.set(TOKEN_KEY, userData).then(res => { 
      // this.authenticationState.next(userData);
    // });
    let userinfo = userData;
    this.storage.set(TOKEN_KEY, userData).then(() => {
      this.dataService.loadUserData();
    })
    console.log('login data set');
    this.authenticationState.next(userinfo);
    return of(userinfo);
  }

  toggleState(res) {
    this.authenticationState.next(res);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }
  
}
